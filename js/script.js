class NewUser {
    constructor(firstName, lastName) {
        this._firstName = firstName;
        this._lastName = lastName
    }

    login = function () {
        const firstChar = this._firstName.at(0).toLowerCase();
        return firstChar + this._lastName.toLowerCase()
    }

    get firstName() {
        return this._firstName
    }

    set firstName(value) {
        if (typeof value !== "string" || value.length === 0) {
            console.warn("You must use not empty string for `firstName` field")
            return
        }
        this._firstName = value
    }

    get lastName() {
        return this._lastName
    }

    set lastName(value) {
        if (typeof value !== "string" || value.length === 0) {
            console.warn("You must use not empty string for `lastName` field")
            return
        }
        this._lastName = value
    }
}

function createNewUser(firstName, lastName) {
    return new NewUser(firstName, lastName)
}

let firstName = prompt("Enter first name")
let lastName = prompt("Enter last name")

while (firstName.length === 0 || lastName.length === 0) {
    alert("You must enter not empty string")
    firstName = prompt("Enter first name")
    lastName = prompt("Enter last name")
}


const user = createNewUser(firstName, lastName)
console.log(user.login())

user.firstName = 123
console.log(user.firstName)

user.lastName = ""
console.log(user.lastName)

console.log(user)